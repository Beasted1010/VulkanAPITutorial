
@ echo off

cd obj

gcc -c -D DEBUG^
       -I C:\SDKs\VulkanSDK\1.0.65.1\Include^
       -I C:\SDKs\glfw-3.2.1.bin.WIN32\include^
       -I C:\SDKs\glm^
          -Wall ../src/main.cpp

cd ../bin

gcc -o vk^
        -L C:\SDKs\VulkanSDK\1.0.65.1\Lib32^
        -L C:\SDKs\glfw-3.2.1.bin.WIN32\lib-mingw-w64^
           ../obj/main.o^
            -lstdc++ -lvulkan-1 -lglfw3 -lgdi32

cd ..
