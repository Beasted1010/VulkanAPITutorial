#define GLFW_INCLUDE_VULKAN // Tells glfw to include its own defintions and automatically load the Vulkan header with it
#include <GLFW/glfw3.h>

#include <iostream>
#include <stdexcept>
#include <functional>

#include <vector>
#include <set>
#include <algorithm>

#include <cstring>

/* TODO TODO TODO:
    WHERE I LEFT OFF:
        Finished Swap chain https://vulkan-tutorial.com/Drawing_a_triangle/Presentation/Swap_chain

        Now on Image views: https://vulkan-tutorial.com/Drawing_a_triangle/Presentation/Image_views

   TODO TODO TODO
*/

/* SUMMARY POINTS
    We use glfw for events, window operations, and the like
    We use Vulkan to access the GPU for GPU related computations

    Structure (excluding validation layers):
        Initialize glfw
            glfwInit
        Initialize window
            glfwWindowHint, glfwCreateWindow, glfwGetRequiredInstanceExtensions
        Main loop -> Poll events
            glfwPollEvents
        Initialize Vulkan
            vkCreateInstance, vkEnumerateInstanceExtensionProperties
        Select graphics card(s)
            Basically: List graphics cards, decide which one (or many) has the features we want, select
            vkEnumeratePhysicalDevices, isDeviceSuitable, vkGetPhysicalDeviceProperties, vkGetPhysicalDeviceFeatures
        Find 1 or more queue families that supports what functionality we want for the physical device
            findQueueFamilies, vkGetPhysicalDeviceQueueFamilyProperties
        Set up logical device to interface with the physical device
            Similar creation process to creating the Vk instance (describe features we want to use)
            createLogicalDevice, vkCreateDevice, vkGetDeviceQueue

        Window surface creation -> Vulkan is platform agnostic, so need "Window System Integration" extensions
            One extension is the VK_KHR_surface -> Exposes VkSurfaceKHR that allows us to render images to
            Need to also adjust isDeviceSuitable to ensure we have a device that can render images to our surface
            Since presentation is a queue-specific feature, we also need to adjust findQueueFamilies and the QueueFamily struct
            Also need to change createLogicalDevice to create the presentation queue and retrieve the VkQueue handle
            glfwCreateWindowSurface

        Swap Chain support -> A swap chain is a buffer of images to be rendered to the screen
            You must create it explicitly in vulkan -> And ensure your graphics card has swap chain support (not all do)
            Since Vulkan does not natively support window systems and surfaces -> You must enable VK_KHR_swapchain (device extension)
            Need to adjust isDeviceSuitable to now check if we have swap chain support
            We also need to check 3 kinds of properties (in the form of structures and list of structures)
                Basic surface capabilities (min/max number of images in swap chain, min/max width and height of images)
                Surface formats (pixel format, color space)
                Available presenting modes
            Then we can fine tune our swap chain for the best selection, this is because there may be many different modes of varying optimality
                There are 3 types of settings to determine
                    Surface format (color depth)
                    Presentation mode (conditions for "swapping" images to the screen)
                    Swap extent (resolution of images in swap chain)
            vkEnumerateDeviceExtensionProperties, vkGetPhysicalDeviceSurfaceCapabilitiesKHR,
                vkGetPhysicalDeviceSurfaceFormatKHR, vkGetPhysicalDevicesSurfacePresentModesKHR,
                chooseSwapPresentMode, chooseSwapSurfaceMode, chooseSwapExtent -> Our 3 helper functions for settings of swap space
                createSwapChain -> Use our helper functions (querySwapChainSupport included) to create swap chain
                vkCreateSwapchainKHR, vkDestroySwapchainKHR

        Cleanup
            vkDestroyDevice, DestroyDebugReportCallbackEXT, vkDestroyInstance, glfwDestroyWindow, glfwTerminate

*/

/* TODO's and NOTE's
    ValidationLayers don't seem to be working. debugCallback does not seem to be called

    // * is used to indicate an insightful moment I had, also indicates comments that ties other things together

*/

// A vector of validation layers that we wish to use
const std::vector<const char*> validationLayers = {"VK_LAYER_LUNARG_standard_validation"};

// A vector of device extensions we require -> Currently we want swap chain support
const std::vector<const char*> deviceExtensions = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};

// Debug mode
#ifdef DEBUG
    #define DEBUG_ON true
    const bool enableValidationLayers = true;
#else
    #define DEBUG_ON false
    const bool enableValidationLayers = false;
#endif

const int WIDTH = 800;
const int HEIGHT = 600;

// A graphicsFamily && presentFamily queue index of -1 indicates queue family is "not found"
struct QueueFamilyIndices {
    // Possible for queue families to be disjoint in the sense that one supports drawing commands but not presentation or vica versa
    // Which is why we want to ensure our queue family supports BOTH graphics and presentation
    // NOTE: It is very likely that these 2 queue families are the same, but we will treat them as separate for a uniform approach
    int graphicsFamily = -1;
    int presentFamily = -1;

    bool isComplete() {
        return graphicsFamily >= 0 && presentFamily >= 0;
    }
};

struct SwapChainSupportDetails {
    VkSurfaceCapabilitiesKHR capabilities;
    std::vector<VkSurfaceFormatKHR> formats;
    std::vector<VkPresentModeKHR> presentModes;
};

// Because vkCreateDebugReportCallbackEXT is an extension function, it is not automatically loaded
// So we have to lookup its address ourselves (since it's not automatically loaded)
VkResult CreateDebugReportCallbackEXT(VkInstance instance, const VkDebugReportCallbackCreateInfoEXT* pCreateInfo,
                                      const VkAllocationCallbacks* pAllocator, VkDebugReportCallbackEXT* pCallback) {
    /*This is a proxy function (intermediary) that handles the lookup of the extension function address ourselves*/

    // Grab the address of the desired function we are after, cast it to the function type that has our return type and args
    auto func = (PFN_vkCreateDebugReportCallbackEXT) vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");

    // If func is NULL, then the desired function couldn't be loaded
    if( func != NULL )
        return func(instance, pCreateInfo, pAllocator, pCallback);
    else
        return VK_ERROR_EXTENSION_NOT_PRESENT;
}

// Similar to the above, a proxy function for vkDestroyDebugReportCallbackEXT to destroy the callback object
void DestroyDebugReportCallbackEXT(VkInstance instance, VkDebugReportCallbackEXT callback,
                                                       const VkAllocationCallbacks* pAllocator) {
    /*Proxy function (intermediary) that handles the lookup of the extension function address ourselves*/

    auto func = (PFN_vkDestroyDebugReportCallbackEXT) vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");

    if (func != NULL)
        func(instance, callback, pAllocator);
}

class HelloTriangleApplication {
public:
    void run() {
        initWindow();
        initVulkan();
        mainLoop();
        cleanup();
    }

private:
    // Used to initialize GLFW and create a window
    void initWindow() {
        if( !glfwInit() ) // Initialize the GLFW library
        {
            std::cerr << "glfw failed to initialize!\n";
            exit(1);
        }

        /* glfwInit will set window hints to their default values
            These hints can be set individually with glfwWindowHint
            They can also be returned to their defaults with glfwDefaultWindowHints */
        /* Hints are things that determine attributes, constraints/guidelines, and properties of the specific aspect
            Hints can be related to: Windows, Framebuffers, Monitors, or Context (all of which relate to window behavior)
            These can affect, e.g., the window itself, the framebuffer, the context, etc.
                e.g. GLFW_VISIBLE (window initially visible?), GLFW_RESIZABLE (window resizable?)
                e.g. GLFW_RED_BITS, GLFW_GREEN_BITS, GLFW_ALPHA_BITS, GLFW_DEPTH_BITS, etc. (specify framebuffer info)
                e.g. GLFW_REFRESH_RATE (specify desired refresh rate for a full screen window)
            Hints can specify hard Constraints or not-hard constraints
                Hard constraints: Result must match EXACTLY with these constraints to be successful
                Not-hard constraints: Result matches these constraints as closely as possible but may differ */
        // FPs(int hint, int value) -> Sets one hint at a time for the next call to glfwCreateWindow
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API); // Set GLFW_CLIENT_API to indicate we don't wish to use OpenGL API
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE); // We want to give special care to resizing a window later

        // FPs(int width, int height, const char* title, GLFWmonitor* monitor, GLFWwindow* share)
        window = glfwCreateWindow( WIDTH, HEIGHT, "Vulkan", NULL, NULL ); // Returns: Window handle or NULL if error occured

        if( !window )
        {
            std::cerr << "Window failed to create!\n";
            exit(1);
        }

    }

    void createInstance() {
        // If we wanted to use validation layers but we don't have the support, then throw exception
        if(enableValidationLayers && !checkValidationLayerSupport())
            throw std::runtime_error("Validation layers requested, but not available!\n");

        // Some information about our application, this is optional, but may be provide useful information to the driver
        VkApplicationInfo appInfo = { // A structure
            .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO, // The type of this structure VkStructureType enum
            .pNext = NULL, // Must be NULL
            .pApplicationName = "Hello Triangle", // Name of the application
            .applicationVersion = VK_MAKE_VERSION(1, 0, 0), // Make the version for our application (this is a macro)
            .pEngineName = "No Engine", // Name of the engine used to create application
            .engineVersion = VK_MAKE_VERSION(1, 0, 0), // FPs(major, minor, patch)
            .apiVersion = VK_API_VERSION_1_0
        };

        /* A required struct, tells Vulkan driver which global (apply to entire program, not specific device)
                                                            extensions and validation layers we want to use */
        VkInstanceCreateInfo createInfo = {}; // Initialize all members to 0
        createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO; // Specificy the structure type
        createInfo.pApplicationInfo = &appInfo; // Helps implementations recognize behavior inherent to classes of applications

        /* Note that Vulkan is platform agnostic (meaning it runs on any OS), so we need an extension for window operations
            GLFW ahs a build-in function that returns the extension(s) it needs to interface with the window system */
        uint32_t glfwExtensionCount = 0; // Store how many extensions our platform needs to operate on windows
        const char** glfwExtensions; // The extension(s) we need

        // If we desire the insight, we can see an explicit listing of the possible extensions available
        if( DEBUG_ON )
        {
            uint32_t extensionCount = 0;
            // FPs(const char* layerName, uint32_t* propertyCount, VkExtensionProperties properties)
            // Returns up to requested number of global extension properties
            // Used to query the available instance extensions (e.g. window system interface)
            // NOTE: properties argument is NULL because we want to get # of available extension properties first
            vkEnumerateInstanceExtensionProperties(NULL, &extensionCount, NULL);

            // An array to hold the extension details
            std::vector<VkExtensionProperties> extensions(extensionCount);

            // Query the extension details for the given extensions
            vkEnumerateInstanceExtensionProperties(NULL, &extensionCount, extensions.data());

            // Each VkExtensionProperties struct contains name and version of an extension
            // Use a range based for loop. This requires C++11, used to provide a more readable loop over a range of elements
            for(const auto &extension : extensions)
                std::cout << "\t" << extension.extensionName << std::endl;
        }

        // Returns array of names of Vulkan instance extensions required by GLFW for creating Vulkan surfaces for GLFW windows
        // If successful, the list will contain VK_KHR_surface, otherwise returns NULL
        // FPs(count) -> Where to store the # of extensions in the returned array, the location of extension count
        glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

        // These fields determine the global (apply to entire program, not specific device) validation layers to enable
        createInfo.enabledExtensionCount = glfwExtensionCount;
        createInfo.ppEnabledExtensionNames = glfwExtensions;

        // NOTE: The below 3 lines were added as part of validation layers implementation (including both for completeness)
        auto extensions = getRequiredExtensions();
        createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
        createInfo.ppEnabledExtensionNames = extensions.data();

        if(enableValidationLayers) {
            createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
            createInfo.ppEnabledLayerNames = validationLayers.data();
        }
        else
            createInfo.enabledLayerCount = 0;

        // Create an instance of Vulkan
        if( vkCreateInstance(&createInfo, NULL, &instance) != VK_SUCCESS )
            throw std::runtime_error("Failed to create instance!\n");
    }

    void initVulkan() {
        createInstance();
        setupDebugCallback();
        createSurface();
        pickPhysicalDevice();
        createLogicalDevice();
        createSwapChain();
    }

    void mainLoop() {

        // Event loop
        while( !glfwWindowShouldClose(window) ) // Function returns the close flag (1/0) -> Upon close flag=1
        {
            glfwPollEvents(); // Process events that are in the event queue
        }

    }

    void createSurface() {
        /*Create the window surface we will be using*/

        /* Some behind the scenes insights to show what the function we actually use is doing. Example for Windows
            // If Linux, then xvkCreateXcbSurfaceKHR takes an XCB connection & window as creation details with X11
            // The glfwCreateWindowSurface function performs EXACTLY the same operations as below

            // A window surface is a Vulkan object, so it comes with a VkWin32SurfaceCreateInfoKHR struct to fill it
            // Two important paramters -> hwnd and hinstance -> These are handles to the window and the process
            VkWin32SurfaceCreateInfoKHR createinfo;
            createInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
            createInfo.hwnd = glfwGetWin32Window(window); // Get the raw HWND for the GLFW window object
            createInfo.hinstance = GetModuleHandle(nullptr); // Get the HINSTANCE handle of the current process

            // Can now create the surface with vkCreateWin32SurfaceKHR (needs to be explicitly loaded again)
            auto CreateWin32SurfaceKHR = (PFN_vkCreateWin32SurfaceKHR) vkGetInstanceProcAddr(instance, "vkCreateWin32SurfaceKHR");
            if (!CreateWin32SurfaceKHR || CreateWin32SurfaceKHR(instance, &createInfo, nullptr, &surface) != VK_SUCCESS)
                throw std::runtime_error("failed to create window surface!");
        */

        // FPs(VkINstance, GLFW window pointer, customer allocators, pointer to VkSurfaceKHR)
        if(glfwCreateWindowSurface(instance, window, NULL, &surface) != VK_SUCCESS)
            throw std::runtime_error("Failed to create window surface!\n");
    }

    void createLogicalDevice() {
        /*Create the logical device for our physical device*/

        // Find queue families for our physical device
        QueueFamilyIndices indices = findQueueFamilies(physicalDevice);

        // An elegant means to fill multiple structures to create a queue from both families
        std::vector<VkDeviceQueueCreateInfo> queueCreateInfos; // Multiple queue infos
        std::set<int> uniqueQueueFamilies = {indices.graphicsFamily, indices.presentFamily}; // Two queue families

        // Vulkan allows us to assign properties to queues to influence the scheduling of command buffer execution
        // This influence is done by floating point numbers between 0.0 and 1.0 -> Still required for a single queue
        float queuePriority = 1.0f;

        for(int queueFamily : uniqueQueueFamilies) { // Range-based loop to iterate over each of our queue families
            // Details for our queue
            VkDeviceQueueCreateInfo queueCreateInfo = {};
            queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            queueCreateInfo.queueFamilyIndex = indices.graphicsFamily;
            queueCreateInfo.queueCount = 1; // Only need one queue, since we can use multiple threads for our command buffers
            queueCreateInfo.pQueuePriorities = &queuePriority;
            queueCreateInfos.push_back(queueCreateInfo); // Insert the newly created queue info into our vector
        }

        // Set the device families that we will be using
        // Same features that we queried support for with vkGetPhysicalDeviceFeatures (e.g. geometry shaders)
        VkPhysicalDeviceFeatures deviceFeatures = {}; // Currently don't need anything special

        // Now we can start filling out information in the main VkDeviceCreateInfo structure
        VkDeviceCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;

        // Add pointers to the queue creation info and device features structs
        createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size()); // However many queues we have
        createInfo.pQueueCreateInfos = queueCreateInfos.data();

        createInfo.pEnabledFeatures = &deviceFeatures;

        // The rest will be similiar to VkInstanceCreateInfo but now these are device specific
        // e.g. of a device specific extension is VK_KHR_swapchain -> allows you to present rendered images from device to windows

        // Enable our extensions
        createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
        createInfo.ppEnabledExtensionNames = deviceExtensions.data();

        // Just like before with the validation layers
        if(enableValidationLayers) {
            createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
            createInfo.ppEnabledLayerNames = validationLayers.data();
        }
        else
            createInfo.enabledLayerCount = 0;

        // Paramaters: Physical device to interface with, queue and usage info we just specified,
                //optional allocation callback pointer, and a pointer to a variable to store the logical device handle in
        if(vkCreateDevice(physicalDevice, &createInfo, NULL, &device) != VK_SUCCESS)
            throw std::runtime_error("Failed to create logical device!\n");

        // If the queue families are the same, then the two handles will most likely have the same value
        vkGetDeviceQueue(device, indices.graphicsFamily, 0, &graphicsQueue);
        vkGetDeviceQueue(device, indices.presentFamily, 0, &presentQueue);

    }

    QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device) {
        /*Find queue families that are supported by the device and which of these we wish to use*/

        // The indices for our queue family
        QueueFamilyIndices indices;

        // Same process as all others to retrieve a list of queue families
        uint32_t queueFamilyCount = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, NULL); // Get the count first

        // The VkQueueFamilyProperties structs contains some details about the queue family
        // Including the type of operations that are supported and the number of queues that can be created based on that family
        std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

        int i = 0;
        // Find at least 1 queue famliy that has what we want (currenlty that is graphic command support)
        for(const auto& queueFamily : queueFamilies) { // Range-based loop to iterate over each queueFamily
            // If we have at least 1 queue and the queue family supports graphics commands, then add that index
            if(queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT)
                indices.graphicsFamily = i;

            // Check to see if the queue family is capable of presenting to our window surface
            VkBool32 presentSupport = false;
            vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);

            if(queueFamily.queueCount > 0 && presentSupport)
                indices.presentFamily = i;

            // If the queue family index is not -1 anymore, meaning that we have found a graphics family that we want
            if(indices.isComplete())
                break;

            // Otherwise move onto the next graphics family
            i++;
        }

        return indices;
    }

    // NOTE: Function may be updated as we get new features to add (e.g. device memory, queue families, etc.)
    bool isDeviceSuitable(VkPhysicalDevice device) {
        /*Check to see the device has the desired operations that we will be performing on the GPU*/

        // NOTE: The below "GetPhysicalDeviceFeatures" is currently unused
        // We begin by retrieving the properties of the device
        VkPhysicalDeviceProperties deviceProperties;
        vkGetPhysicalDeviceProperties(device, &deviceProperties);

        // Check support for optional features such as texture compression, 64 bit floats, and multi viewport rendering (VR)
        VkPhysicalDeviceFeatures deviceFeatures;
        vkGetPhysicalDeviceFeatures(device, &deviceFeatures);


        // NOTE: Below are a couple different ways that you could implement the device selection process
        // You could also (of course) just display the names of the devices (optionally showing features) and let the user select
        /* EXAMPLE for checking if our our selected graphics card(s) supports geometry shaders
            return deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU && deviceFeature.geometryShader;
        */
        /* EXAMPLE for implementing some sort of score for each graphics card to determine best one to use
        #include <map>
        ...

        void pickPhysicalDevice() {
        ...

        // Use an ordered map to automatically sort candidates by increasing score
        std::multimap<int, VkPhysicalDevice> candidates;

        for (const auto& device : devices) {
            int score = rateDeviceSuitability(device);
            candidates.insert(std::make_pair(score, device));
        }

        // Check if the best candidate is suitable at all
        if (candidates.rbegin()->first > 0) {
            physicalDevice = candidates.rbegin()->second;
        } else {
            throw std::runtime_error("failed to find a suitable GPU!");
        }
        }

        int rateDeviceSuitability(VkPhysicalDevice device) {
        ...

        int score = 0;

        // Discrete GPUs have a significant performance advantage
        if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
            score += 1000;
        }

        // Maximum possible size of textures affects graphics quality
        score += deviceProperties.limits.maxImageDimension2D;

        // Application can't function without geometry shaders
        if (!deviceFeatures.geometryShader) {
            return 0;
        }

        return score;
        }

        */

        // Look for a queue family that has what we want
        QueueFamilyIndices indices = findQueueFamilies(device);

        // Check if our desired extensions for the device is supported
        bool extensionsSupported = checkDeviceExtensionSupport(device);

        bool swapChainAdequate = false;
        if(extensionsSupported) {
            SwapChainSupportDetails swapChainSupport = querySwapChainSupport(device);
            swapChainAdequate = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
        }

        // Did we find a queue family that we want?
        return indices.isComplete() && extensionsSupported && swapChainAdequate;
    }

    bool checkDeviceExtensionSupport(VkPhysicalDevice device) {
        /*Check if our device supports the desired extensions*/

        // Count the number of extensions we have available -> Just like we counted devices and such in other functions
        uint32_t extensionCount;
        vkEnumerateDeviceExtensionProperties(device, NULL, &extensionCount, NULL);

        // Now we are ready for part 2, fill the data for each of the counted device extension properties found in above
        std::vector<VkExtensionProperties> availableExtensions(extensionCount);
        vkEnumerateDeviceExtensionProperties(device, NULL, &extensionCount, availableExtensions.data());

        // List of the required extensions (using the vector/list from global scope)
        std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());

        // Treat requiredExtensions as a checklist, go through the extensions that are available and check off what we have
        for(const auto& extension : availableExtensions)
            requiredExtensions.erase(extension.extensionName);

        return requiredExtensions.empty();
    }

    void createSwapChain() {
        /*Use the helpers functions to create our swap chain*/

        // Get the swap chain support device, fill out the swap chain support details
        SwapChainSupportDetails swapChainSupport = querySwapChainSupport(physicalDevice);

        // Get our 3 settings for the swap chain
        VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);
        VkPresentModeKHR presentMode = chooseSwapPresentMode(swapChainSupport.presentModes);
        VkExtent2D extent = chooseSwapExtent(swapChainSupport.capabilities);

        // A quick additional decision to make regarding swap chain.. The number of images in the swap chain (i.e. queue length)
        // Note: maxImageCount = 0 means there is no limit (besides memory requirements) -> So check that too
        uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1; // 1 more to ensure proper tripple buffering implementation
        if(swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount)
            imageCount = swapChainSupport.capabilities.maxImageCount;

        // Create the swap chain object -> Requires filling out a large structure
        VkSwapchainCreateInfoKHR createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        createInfo.surface = surface; // The surface the swap chain should be tied to

        // Details of the swap chain image...
        createInfo.minImageCount = imageCount; // Count of VkImage's
        createInfo.imageFormat = surfaceFormat.format;
        createInfo.imageColorSpace = surfaceFormat.colorSpace;
        createInfo.imageExtent = extent;
        createInfo.imageArrayLayers = 1; // Number of layers each image consists of (always 1 unless developing a steroscopic 3D application)
        // Specify what kind of operations we'll use the images in the swap chain for
        // We are going to be rendering directly to them (so they're used as color attachment)
        // Note: Also possible that you'll render images to a separate image first to perform operations like post processing
                // In this case you would use VK_IMAGE_USAGE_TRANSFER_DST_BIT
                // And instead use a memory operation to transfer the rendered image to a swap chain image
        createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;


        QueueFamilyIndices indices = findQueueFamilies(physicalDevice);
        uint32_t queueFamilyIndices[] = {(uint32_t) indices.graphicsFamily, (uint32_t) indices.presentFamily};

        // Now need to specify how to handle swap chain images that will be used across multiple queue families
        // That will be the case in our application if the graphics queue family is different from the presentation queue

        // NOTE: We will be drawing images in the swap chain from graphics queue, and then submitting them on the presentation queue
        // Two ways to handle images that are accessed from multiple queues
            // VK_SHARING_MODE_CONCURRENT: Images can be used across multiple queue families without explicit ownership transfers
                // Requires specification in advance between which queue families ownership will be shared,
                    // using queuefamilyIndexCount and pQueueFamilyIndices parameters
            // VK_SHARING_MODE_EXCLUSIVE: An image is owned by one queue family at a time, ownership must be explicitly transfered
                // before using it in another queue family. This option offers the BEST performance

        if(indices.graphicsFamily != indices.presentFamily) { // graphics queue family is different from presentation queue family
            createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
            createInfo.queueFamilyIndexCount = 2;
            createInfo.pQueueFamilyIndices = queueFamilyIndices;
        }
        else { // For our cases (as is with most hardware), currently, the queue families should be the same
            // Our current choice, concurrent mode would require specification of at least 2 distinct queue families
            createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
            createInfo.queueFamilyIndexCount = 0; // Optional (done by default)
            createInfo.pQueueFamilyIndices = NULL; // Optional
        }

        // Specifying a certain transform that we wish to apply to images in the swap chain (if supported... supportedTransforms in capabilities)
        // Such as a 90 degree clockwise rotation or a horizontal flip.
        // If you don't want any transformation, simply specify the current transformation (as we do)
        createInfo.preTransform = swapChainSupport.capabilities.currentTransform;

        // compositeAlpha field specifies if the alpha channel should be used for blending with other windows in the window system
        createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR; // Ignore alpha channel (almost always desired)

        createInfo.presentMode = presentMode; // Our present mode we decided on
        // If clipped=true, then we don't care about the color of pixels that are obscured (e.g. another window is infront of them) -> Best for performance
        createInfo.clipped = VK_TRUE; // This is fine unless you really need to be able to read these pixels back and get predicted results

        // The last field... Used to provide reference to old swap chain after a new swap chain is created
        // Possible for swap chain to become invalid or unoptimized (e.g. window resize) -> New swap chain created from scratch
        createInfo.oldSwapchain = VK_NULL_HANDLE;

        if(vkCreateSwapchainKHR(device, &createInfo, NULL, &swapChain) != VK_SUCCESS)
            throw std::runtime_error("Failed to create swap chain!\n");

        // Retrieve the handles of the VkImage's
        vkGetSwapchainImagesKHR(device, swapChain, &imageCount, NULL);
        // We already specified the desired images in minImageCount of createInfo,
        // the imlementation is allowed to create more images, which is why we needed to explicitly query the amount again
        swapChainImages.resize(imageCount);
        vkGetSwapchainImagesKHR(device, swapChain, &imageCount, swapChainImages.data());

        // At this point we have a set of images that can be draw onto and be preseneted to the window

        // The below is for future development in later parts of tutorial
        VkSwapchainKHR swapChain;
        std::vector<VkImage> swapChainImages;
        VkFormat swapChainImageFormat;
        VkExtent2D swapChainExtent;

        // Stuf here...


        swapChainImageFormat = surfaceFormat.format;
        swapChainExtent=extent;

    }

    SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device) {
        /*Populate the SwapChainSupportDetails structure, the filled structure is used to ensure necessary detail support on device*/
        SwapChainSupportDetails details;

        // Query for basic surface capabilities
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &details.capabilities);

        // Query the supported surface formats
        // Same 2 function call structure as other requests to fill a list of structs
        uint32_t formatCount; // Count number of formats
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, NULL);

        if(formatCount != 0) {
            // First resize the vector to now-known size
            details.formats.resize(formatCount);

            // Now fill the format structure
            vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, details.formats.data());
        }

        // Query the supported presentation modes
        // Same 2 function call structure
        uint32_t presentModeCount;
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, NULL); // First, count

        if(presentModeCount != 0) {
            details.presentModes.resize(presentModeCount); // Resize vector to now-known presentModeCount
            vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, details.presentModes.data());
        }

        // Now all the details we need are in the "details" SwapChainSupportDetails structure
        return details;
    }

    // Each VkSurfaceFormatKHR contains ...
        // format -> specify color channels and types
            //e.g. VK_FORMAT_B8G8R8A8_UNORM -> Meaning we store B G R and alpha channels in that order w/ an 8 bit unsigned int for a total of 32 bits per pixel
        // colorSpace -> Indicates if SRGB is supported using VK_COLOR_SAPCE_SRGB_NONLINEAR_KHR flag
            // We will use SRGB for colorSpace, if available -> Results in more accurate perceived colors
            // But working with SRGB is a bit more challenging, so we'll stick to standard RGB color format
    VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) {
        /*Fine tune our selection of swap space with a format and colorSpace*/

        // Search for our selection...

        // 1: No preference
        // This is the case where the surface has no preferred format -> Indicated by VK_FORMAT_UNDEFINED in VkSurfaceFormatKHR
        if(availableFormats.size() == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED)
            return {VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR};

        // 2: Preference is included in availableFormats already
        // Go through the list of availableFormats and see if the prefered combination is available
        for(const auto& availableFormat : availableFormats)
            // Search for a format that matches our preference, if it exists, then grab it
            if(availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
                return availableFormat;

        // 3: Rank available formats based on how "good" they are
        // ... But we will just settle with the first format specified
        return availableFormats[0];
    }

    VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR> availablePresentModes) {
        /*choose a present mode based off of what is available and what we desire*/

        VkPresentModeKHR bestMode = VK_PRESENT_MODE_FIFO_KHR; // We will settle if need be

        // Let's aim for the tripple buffering support (less tearing) of VK_PRESENT_MODE_MAILBOX_KHR (not all drivers support)
        // Search through our available present modes and see if our desired mode is available, select if so
        for(const auto& availablePresentMode : availablePresentModes) {
            if(availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR)
                return availablePresentMode;
            // Our next preference would be VK_PRESENT_MODE_IMMEDIATE_KHR if MAILBOX doesn't exist
            else if(availablePresentMode == VK_PRESENT_MODE_IMMEDIATE_KHR)
                bestMode = availablePresentMode;
        }

        return bestMode;
    }

    VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities) {
        /*Determine the resolution we wish for the images in the swap chain
          Default is often the same as our window we render to*/

        // If we are allowed to differ the values of width and height from that of the window we render to,
        // then do so by setting the actualExtent to our desired WIDTH and HEIGHT globals
        // Note that the uint32_t is a special value of currentExtent width, indicating we are allowed variance from window resolution
        if(capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max())
            return capabilities.currentExtent;
        else // We are allowed variance
        {
            VkExtent2D actualExtent = {WIDTH, HEIGHT};

            /*Min/Max functions are used here to clamp the value of WIDTH and HEIGHT
              between the allowed min and max extents that are supported by the implementation*/
            actualExtent.width = std::max( capabilities.minImageExtent.width,
                std::min(capabilities.maxImageExtent.width, actualExtent.width) );

            actualExtent.height = std::max( capabilities.minImageExtent.height,
                std::min(capabilities.maxImageExtent.height, actualExtent.height) );

            return actualExtent;
        }
    }


    void pickPhysicalDevice() {
        /*Pick the graphics card (that satisfies our feature requirements) that we wish to use*/

        //* Similar to listing extensions, we will list the graphics cards by first querying just the number
        // Passing NULL indicates that we are after just the number of graphic cards
        uint32_t deviceCount = 0;
        vkEnumeratePhysicalDevices(instance, &deviceCount, NULL);

        if(deviceCount == 0)
            throw std::runtime_error("Failed to find GPUs with Vulkan support!\n");

        // Now we are ready to create the array to hold all the VkPhysicalDevice handles
        std::vector<VkPhysicalDevice> devices(deviceCount); // We have deviceCount many devices
        vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());

        // We need to evaluate each of the devices to see if they are suitable for the operations we want to perform
        // This is needed because not all graphics cards are created equal, some might not be able to do certain operations
        for(const auto& device : devices) { // Range-based loop to iterate all of our available devices
            // Check our current device
            if(isDeviceSuitable(device)) {
                physicalDevice = device;
                break;
            }
        }

        // If we haven't added a different device then we don't have any compatable for our desired operations
        if(physicalDevice == VK_NULL_HANDLE)
            throw std::runtime_error("Failed to find a suitable GPU!\n");

    }

    // A function Vulkan will call
    // Return result is whether or not the call that triggered the validation layer message should be aborted
    // VKAPI_ATTR and VKAPI_CALL are used to ensure that the function has the right signature for Vulkan to call it
    static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback( // NOTE: VkBool32 is the return type
        VkDebugReportFlagsEXT flags,            // Specifies type of message (any combination of VK_DEBUG_REPORT bit flags)
        VkDebugReportObjectTypeEXT objType,     // Specifies type of object that is subject of message
        uint64_t obj,                           // The object of message (e.g. VkPhysicalDevice)
        size_t location,
        int32_t code,
        const char* layerPrefix,
        const char* msg,                        // Contains pointer to the message itself
        void* userData                          // Paramater to pass your own data to the callback
    ) {
        /*Define what is to happen when we call debugCallback during case of the validation layer being enabled*/
        std::cerr << "Validation layer: " << msg << std::endl;

        // Stating that the call that triggered the validation layer message should be aborted
        // VK_TRUE -> Yes, call should be aborted with VK_ERROR_VALIDATION_FAILED_EXT error
                    // Often used to test the validation layers themself, so more common to return VK_FALSE
        return VK_FALSE;
    }

    void setupDebugCallback() {
        // Only continue executing this function if we have validation layers enabled
        if(!enableValidationLayers)
            return;

        // Structure with details about the callback
        VkDebugReportCallbackCreateInfoEXT createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
        createInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT; // Allow for message filtering
        createInfo.pfnCallback = debugCallback; // Specify pointer to the callback function
        // Could also pass pointer to pUserData field which will be pased along to the callback function via userData parameter
                // This could be used to pass a pointer to the HelloTriangleApplication class, for example

        // The NULL argument is for the allocator parameter, which we are not considering for this tutorial
        if(CreateDebugReportCallbackEXT(instance, &createInfo, NULL, &callback) != VK_SUCCESS)
            throw std::runtime_error("Failed to set up debug callback!\n");
    }

    bool checkValidationLayerSupport(){
        /*Determine if we have validation layer support for debugging purposes. List the layers we have*/

        // Count the number of layers that we have available
        uint32_t layerCount;
        // NOTE: properties argument is NULL because we want to get # of available extension properties first
        vkEnumerateInstanceLayerProperties(&layerCount, NULL);

        // An array to hold the available layer details
        std::vector<VkLayerProperties> availableLayers(layerCount);
        vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

        // Check to see if validationLayers exist in availableLayers ->
        // C++11 Range-based for loop -> for( range_declaration: range_expression) {...}
        // Essentially -> "for range_declaration IN range_expression" -> Defining a name to refer to each entry
        for (const char* layerName : validationLayers) {
            bool layerFound = false;

            // auto specifies that the type of the variable will be automatically DEDUCED from its initializer (availableLayers)
            for (const auto& layerProperties : availableLayers) {
                if (strcmp(layerName, layerProperties.layerName) == 0) {
                    layerFound = true;

                    if( DEBUG_ON )
                        printf("Available Validation Layer: %s\n", layerProperties.layerName);

                    break;
                }
            }

            if (!layerFound) {
                return false;
            }
        }

        return true;
    }

    std::vector<const char*> getRequiredExtensions() {
        /*Retrieve required extensions*/

        // Vector to store our extensions
        std::vector<const char*> extensions;

        uint32_t glfwExtensionCount = 0;
        const char** glfwExtensions;

        //Get the required extensions
        glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

        // Populate our extensions vector
        for (uint32_t i = 0; i < glfwExtensionCount; i++)
            extensions.push_back(glfwExtensions[i]);

        // If we have validation layers enabled, then enable receiving debug messages back to our program
        if (enableValidationLayers)
            extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);

        return extensions;
    }

    void cleanup() {
        // Swap chain images are automatically cleaned up once the swap chain is destroyed
        vkDestroySwapchainKHR(device, swapChain, NULL); // Destroy the specified swap chain for the given device
        vkDestroyDevice(device, NULL); // Physical device isn't included because logical devices don't interact directly with instances
        DestroyDebugReportCallbackEXT(instance, callback, NULL);

        vkDestroySurfaceKHR(instance, surface, NULL); // Destroy the surface
        vkDestroyInstance(instance, NULL); // Destroy the instance of Vulkan

        glfwDestroyWindow(window); // Destroy the specified window

        glfwTerminate(); // Terminates the GLFW library -> will destroy any remaining windows, monitor and cursor objects
    }

    GLFWwindow* window; // Our GLFW window
    VkInstance instance; // Our Vulkan instance
    VkDebugReportCallbackEXT callback; // A handle for the debug callback (surprising that it needs one)
    VkSurfaceKHR surface;
    VkPhysicalDevice physicalDevice = VK_NULL_HANDLE; // A handle for our physical device
                                                      // Implicitly destroyed during VkInstance's destruction
    VkDevice device; // Handle to our logical device
    VkQueue graphicsQueue; // Graphics queue. Device queues are implicitly cleaned up when device is destroyed
    VkQueue presentQueue; // Presentation queue.

    VkSwapchainKHR swapChain; // Our swap chain object
    std::vector<VkImage> swapChainImages; // Used to store the handles of VkImage's
};

int main() {
    HelloTriangleApplication app;

    try {
        app.run();
    } catch (const std::runtime_error& e) {
        std::cerr << e.what() << std::endl;
        std::cerr << "Exit Failure!\n";
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
